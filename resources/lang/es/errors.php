<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Error Message Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the Laravel Responder package.
    | When it generates error responses, it will search the messages array
    | below for any key matching the given error code for the response.
    |
    */

    'unauthenticated' => 'Usted no está autenticado para acceder a esta solicitud.',
    'unauthorized' => 'Usted no está autorizado para acceder a esta solicitud.',
    'page_not_found' => 'La página solicitada no existe.',
    'relation_not_found' => 'La relación solicitada no existe.',
    'validation_failed' => 'Los datos proporcionados no pasaron la validación.',
    'sold_out_error' => 'Productos agotados',
];
