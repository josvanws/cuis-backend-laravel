<?php

namespace App;

use App\Transformers\HogarTransformer;
use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;

class Hogar extends Model implements Transformable
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hogares';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function integrantes()
    {
        return $this->hasMany('App\Integrante');
    }

    public function vivienda()
    {
        return $this->belongsTo('App\Vivienda');
    }

    public function transformer()
    {
        return HogarTransformer::class;
    }
}
