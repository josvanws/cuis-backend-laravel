<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoAdministracion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tipo_administraciones';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function carreteras() {
        return $this->hasMany('App\Carretera');
    }
}
