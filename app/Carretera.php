<?php

namespace App;

use App\Transformers\CarreteraTransformer;
use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;

class Carretera extends Model implements Transformable
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function direccion()
    {
        return $this->belongsTo('App\Direccion');
    }

    public function derechoTransito()
    {
        return $this->belongsTo('App\DerechoTransito');
    }

    public function tipoAdministraciones()
    {
        return $this->belongsTo('App\TipoAdministracion');
    }

    public function transformer()
    {
        return CarreteraTransformer::class;
    }
}
