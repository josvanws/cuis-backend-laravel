<?php

namespace App;

use App\Transformers\AsentamientoTransformer;
use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;

class Asentamiento extends Model implements Transformable
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function tipoAsentamiento() {
        return $this->belongsTo('App\TipoAsentamiento');
    }

    /**
     * Get a transformer for the class.
     * @return \Flugg\Responder\Transformers\Transformer|string|callable
     */
    public function transformer()
    {
        return AsentamientoTransformer::class;
    }
}
