<?php


namespace App\Helpers;


class Gealtec
{
    /**
     * @param array $rules origin rules
     * @param array $keys keys to generate rules
     * @param string? $glue
     * @return array Rules generated
     */

    public static function iter_rules($rules, $keys, $glue = "")
    {
        $generated_rules = [];

        if (!empty($glue)) {
            $glue .= ".";
        }

        foreach ($keys as $key) {
            foreach ($rules as $property => $property_rules) {
                $generated_rules[$key . "." . $glue . $property] = $property_rules;
            }
        }
        return $generated_rules;
    }
}
