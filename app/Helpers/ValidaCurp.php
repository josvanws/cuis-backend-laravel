<?php


namespace App\Helpers;


use GuzzleHttp\Client;

class ValidaCurp
{
    private static $peticion =
        '<soapenv:Envelope
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
            xmlns:urn="urn:ValidateMexico">
           <soapenv:Header/>
           <soapenv:Body>
              <urn:Curp soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
             <return xsi:type="urn:CurpReq">
                <user xsi:type="xsd:string">{usuario}</user>
                <password xsi:type="xsd:string">{clave}</password>
                <Curp xsi:type="xsd:string">{curp}</Curp>
             </return>
              </urn:Curp>
           </soapenv:Body>
        </soapenv:Envelope>';

    private static function reemplazarDatos($curp)
    {
        self::$peticion = str_replace('{usuario}', \config('values.ws_validacurp_usuario'), self::$peticion);
        self::$peticion = str_replace('{clave}', \config('values.ws_validacurp_clave'), self::$peticion);
        return str_replace('{curp}', $curp, self::$peticion);
    }

    public static function validar($curp)
    {

        self::$peticion = self::reemplazarDatos($curp);


        $client = new Client([
            'base_uri' => \config('values.ws_validacurp_url'),
            'timeout' => 15.0
        ]);

        $response = $client->request('POST', '', [
            'headers' => [
                'Content-Type' => 'text/xml;charset="utf-8"',
                'Accept-Encoding' => 'application/gzip, deflate',
                'SOAPAction' => '"urn:ValidateMexico#Curp"',
                'Connection' => 'Keep-Alive',
                'Content-lenght' => strlen(self::$peticion)
            ],
            'body' => self::$peticion
        ]);

        $res = $response->getBody()->getContents();
        $dom = new \DOMDocument();
        $dom->loadXML($res);
        $estatus = $dom->getElementsByTagName('Response')->item(0)->nodeValue;
        $resultado = [];
        if ($estatus=='correct') {
            $resultado = array(
                'Estatus' => 1,
                'Curp' => $dom->getElementsByTagName('Curp')->item(0)->nodeValue,
                'Paterno' => $dom->getElementsByTagName('Paterno')->item(0)->nodeValue,
                'Materno' => $dom->getElementsByTagName('Materno')->item(0)->nodeValue,
                'Nombre' => $dom->getElementsByTagName('Nombre')->item(0)->nodeValue,
            );
        }else{
            $resultado = array( 'Estatus' => 0 );
        }

       return $resultado;
    }


}
