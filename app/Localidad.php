<?php

namespace App;

use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;

class Localidad extends Model implements Transformable
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'localidades';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function municipio()
    {
        return $this->belongsTo('App\Muncipio');
    }

    public function direccion()
    {
        return $this->hasMany('App\Direccion');
    }

    public function transformer()
    {
        return LocalidadTransfomer::class;
    }
}
