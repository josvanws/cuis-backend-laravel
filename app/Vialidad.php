<?php

namespace App;

use App\Transformers\VialidadTransformer;
use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;

class Vialidad extends Model implements Transformable
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vialidades';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function tipoVialidades()
    {
       return $this->belongsTo('App\TipoVialidad');
    }

    /**
     * @inheritDoc
     */
    public function transformer()
    {
        // TODO: Implement transformer() method.
        return VialidadTransformer::class;
    }
}
