<?php

namespace App\Exceptions;

use App\Estado;
use Flugg\Responder\Exceptions\Http\HttpException;

class SoldOutException extends HttpException
{
    /**
     * The HTTP status code.
     *
     * @var int
     */
    protected $status = 400;

    /**
     * The error code.
     *
     * @var string|null
     */
    protected $errorCode = 'sold_out_error';

    /**
     * The error message.
     *
     * @var string|null
     */
    protected $message = 'The requested product is sold out.';

    /**
     * Retrieve additional error data.
     *
     * @return array|null
     */
    public function data()
    {
        return [
            'estados' => Estado::all()
        ];
    }
}
