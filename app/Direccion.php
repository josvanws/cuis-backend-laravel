<?php

namespace App;

use App\Transformers\DireccionTransformer;
use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;
use Phaza\LaravelPostgis\Eloquent\PostgisTrait;
use Phaza\LaravelPostgis\Geometries\Point;

/**
 * @property Point ubicacion
 */
class Direccion extends Model implements Transformable
{
    use PostgisTrait;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'direcciones';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $postgisFields = ['ubicacion'];

    protected $postgisTypes = [
        'ubicacion' => [
            'geomtype' => 'geometry',
            'srid' => 4326
        ]
    ];

    public function transformer()
    {
        return DireccionTransformer::class;
    }

    public function vivienda()
    {
        return $this->hasOne('App\vivienda');
    }

    public function localidad()
    {
        return $this->belongsTo('App\Localidad');
    }

    public function vialidad()
    {
        return $this->belongsTo('App\Vialidad');
    }

    public function entre_vialidad1()
    {
        return $this->belongsTo('App\Vialidad');
    }

    public function entre_vialidad2()
    {
        return $this->belongsTo('App\Vialidad');
    }

    public function vialidad_posterior()
    {
        return $this->belongsTo('App\Vialidad');
    }

    public function asentamiento()
    {
        return $this->belongsTo('App\Asentamiento');
    }

    public function camino()
    {
        return $this->belongsTo('App\Camino');
    }

    public function carretera()
    {
        return $this->belongsTo('App\Carretera');
    }


}
