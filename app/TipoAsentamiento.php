<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoAsentamiento extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tipo_asentamientos';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function asentamientos()
    {
        return $this->hasMany('App\Asentamiento');
    }
}
