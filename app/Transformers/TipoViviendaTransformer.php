<?php

namespace App\Transformers;

use App\TipoVivienda;
use Flugg\Responder\Transformers\Transformer;

class TipoViviendaTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param  \App\TipoVivienda $tipoVivienda
     * @return array
     */
    public function transform(TipoVivienda $tipoVivienda)
    {
        return [
            'id' => (int) $tipoVivienda->id,
            'nombre' => $tipoVivienda->nombre
        ];
    }
}
