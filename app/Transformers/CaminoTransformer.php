<?php

namespace App\Transformers;

use App\Camino;
use Flugg\Responder\Transformers\Transformer;

class CaminoTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param  \App\Camino $camino
     * @return array
     */
    public function transform(Camino $camino)
    {
        return [
            'id' => (int) $camino->id,
            'direccion_id' =>$camino->direccion_id,
            'tramo_origen' =>$camino->tramo_origen,
            'tramo_destino' =>$camino->tramo_destino,
            'tipo_margen_id' =>$camino->tipo_margen_id,
            'km' =>$camino->km,
            'metro' =>$camino->metro,
            'tipo_camino_id' => $camino->tipo_camino_id,
        ];
    }
}
