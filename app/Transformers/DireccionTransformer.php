<?php

namespace App\Transformers;

use App\Direccion;
use App\Vialidad;
use Flugg\Responder\Transformers\Transformer;

class DireccionTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [
        'localidad' => LocalidadTransformer::class,
        'vialidad' => VialidadTransformer::class
    ];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [
    ];

    /**
     * Transform the model.
     *
     * @param  \App\Direccion $direccion
     * @return array
     */
    public function transform(Direccion $direccion)
    {
        return [
            'id' => (int) $direccion->id,
            'localidad_id' => $direccion->localidad_id,
            'clave_ageb' => $direccion->clave_ageb,
            'clave_manzana' => $direccion->clave_manzana,
            'vialidad_id' => $direccion->vialidad_id,
            'num_exterior' => $direccion->num_exterior,
            'letra' => $direccion->letra,
            'num_exterior_anterior' => $direccion->num_exterior_anterior,
            'num_interior' => $direccion->num_interior,
            'letra_interior' => $direccion->letra_interior,
            'cp' => $direccion->cp,
            'asentamiento_id' => $direccion->asentamiento_id,
            'entre_vialidad1_id' => $direccion->entre_vialidad1_id,
            'entre_vialidad2_id' => $direccion->entre_vialidad2_id,
            'vialidad_posterior_id' => $direccion->vialidad_posterior_id,
            'referencias' => strtoupper($direccion->referencias),
            'ubiacion' => $direccion->ubicacion ? $direccion->ubicacion->jsonSerialize() : null,
        ];
    }

    /**
     * Include related vialidades.
     *
     * @param  \App\Direccion $direccion
     * @return mixed
     */
    public function includeVialidad(Direccion $direccion)
    {
        //return (new VialidadTransformer)->transform($direccion->vialidad);
        return $direccion->vialidad;
    }
    /**
     * Load shipments with constraints.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function loadVialidad($query)
    {
        return $query->where('tipo_vialidad_id', '=', 9);
    }
}
