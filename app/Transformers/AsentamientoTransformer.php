<?php

namespace App\Transformers;

use App\Asentamiento;
use Flugg\Responder\Transformers\Transformer;

class AsentamientoTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [
        'tipoAsentamiento' => TipoAsentamientoTransformer::class
    ];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param  \App\Asentamiento $asentamiento
     * @return array
     */
    public function transform(Asentamiento $asentamiento)
    {
        return [
            'id' => (int)$asentamiento->id,
            'tipo_asentamiento_id' => (int)$asentamiento->tipo_asentamiento_id,
            'nombre' => $asentamiento->nombre,
        ];
    }
}
