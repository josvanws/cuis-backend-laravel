<?php

namespace App\Transformers;

use App\Parentesco;
use Flugg\Responder\Transformers\Transformer;

class ParentescoTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param  \App\Parentesco $parentesco
     * @return array
     */
    public function transform(Parentesco $parentesco)
    {
        return [
            'id' => (int) $parentesco->id,
            'nombre' => $parentesco->nombre
        ];
    }
}
