<?php

namespace App\Transformers;

use App\TipoAsentamiento;
use Flugg\Responder\Transformers\Transformer;

class TipoAsentamientoTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param  \App\TipoAsentamiento $tipoAsentamiento
     * @return array
     */
    public function transform(TipoAsentamiento $tipoAsentamiento)
    {
        return [
            'id' => (int) $tipoAsentamiento->id,
            'nombre' => $tipoAsentamiento->nombre
        ];
    }
}
