<?php

namespace App\Transformers;

use App\Estado;
use Flugg\Responder\Transformers\Transformer;

class EstadoTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param  \App\Estado $estado
     * @return array
     */
    public function transform(Estado $estado)
    {
        return [
            'id' => (int) $estado->id,
            'nombre' => $estado->nombre
        ];
    }
}
