<?php

namespace App\Transformers;

use App\Localidad;
use Flugg\Responder\Transformers\Transformer;

class LocalidadTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param  \App\Localidad $localidad
     * @return array
     */
    public function transform(Localidad $localidad)
    {
        return [
            'id' => (int) $localidad->id,
            'nombre' => $localidad->nombre,
            'clave' => $localidad->clave
        ];
    }
}
