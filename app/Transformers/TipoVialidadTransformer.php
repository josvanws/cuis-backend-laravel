<?php

namespace App\Transformers;

use App\TipoVialidad;
use Flugg\Responder\Transformers\Transformer;

class TipoVialidadTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param  \App\TipoVialidad $tipoVialidad
     * @return array
     */
    public function transform(TipoVialidad $tipoVialidad)
    {
        return [
            'id' => (int) $tipoVialidad->id,
            'nombre' => $tipoVialidad->nombre
        ];
    }
}
