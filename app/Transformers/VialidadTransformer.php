<?php

namespace App\Transformers;

use App\Vialidad;
use Flugg\Responder\Transformers\Transformer;

class VialidadTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [
        'tipoVialidades' => TipoVialidadTransformer::class
    ];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param  \App\Vialidad $vialidad
     * @return array
     */
    public function transform(Vialidad $vialidad)
    {
        return [
            'id' => (int) $vialidad->id,
            'tipo_vialidad_id' => $vialidad->tipo_vialidad_id,
            'nombre' => $vialidad->nombre
        ];
    }
}
