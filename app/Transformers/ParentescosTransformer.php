<?php

namespace App\Transformers;

use App\Parentescos;
use Flugg\Responder\Transformers\Transformer;

class ParentescosTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param  \App\Parentescos $parentescos
     * @return array
     */
    public function transform(Parentescos $parentescos)
    {
        return [
            'id' => (int) $parentescos->id,
        ];
    }
}
