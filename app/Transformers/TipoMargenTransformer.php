<?php

namespace App\Transformers;

use App\TipoMargen;
use Flugg\Responder\Transformers\Transformer;

class TipoMargenTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param  \App\TipoMargen $tipoMargen
     * @return array
     */
    public function transform(TipoMargen $tipoMargen)
    {
        return [
            'id' => (int) $tipoMargen->id,
        ];
    }
}
