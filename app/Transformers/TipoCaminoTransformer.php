<?php

namespace App\Transformers;

use App\TipoCamino;
use Flugg\Responder\Transformers\Transformer;

class TipoCaminoTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param  \App\TipoCamino $tipoCamino
     * @return array
     */
    public function transform(TipoCamino $tipoCamino)
    {
        return [
            'id' => (int) $tipoCamino->id,
        ];
    }
}
