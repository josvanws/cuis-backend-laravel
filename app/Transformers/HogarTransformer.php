<?php

namespace App\Transformers;

use App\Hogar;
use Flugg\Responder\Transformers\Transformer;

class HogarTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param  \App\Hogar $hogar
     * @return array
     */
    public function transform(Hogar $hogar)
    {
        return [
            'id' => (int) $hogar->id,
            'vivienda_id' => $hogar->vivienda_id,
            'numero_integrantes' => $hogar->numero_integrantes
        ];
    }
}
