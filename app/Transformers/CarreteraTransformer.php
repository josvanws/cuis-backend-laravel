<?php

namespace App\Transformers;

use App\Carretera;
use Flugg\Responder\Transformers\Transformer;

class CarreteraTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param  \App\Carretera $carretera
     * @return array
     */
    public function transform(Carretera $carretera)
    {
        return [
            'id' => (int) $carretera->id,
            'direccion_id' => $carretera->direccion_id ,
            'tipo_administracion_id' => $carretera->tipo_administracion_id ,
            'derecho_transito_id' => $carretera->derecho_transito_id,
            'codigo' => $carretera->codigo ,
            'tramo_origen' => $carretera->tramo_origen,
            'tramo_destino' => $carretera->tramo_destino ,
            'km' => $carretera->km,
            'metro' => $carretera->metro ,
        ];
    }
}
