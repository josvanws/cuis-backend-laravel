<?php

namespace App\Transformers;

use App\Vivienda;
use Flugg\Responder\Transformers\Transformer;

class ViviendaTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [
        'tipoVivienda' => TipoViviendaTransformer::class,
        'direccion' => DireccionTransformer::class
    ];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param  \App\Vivienda $vivienda
     * @return array
     */
    public function transform(Vivienda $vivienda)
    {
        return [
            'id' => (int) $vivienda->id,
            'direccion_id' => (int) $vivienda->direccion_id,
            'tipo_vivienda_id' => (int) $vivienda->tipo_vivienda_id,
            'numero_personas' => (int) $vivienda->numero_personas,
            'numero_hogares' => (int) $vivienda->numero_hogares
        ];
    }
}
