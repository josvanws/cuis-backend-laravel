<?php

namespace App\Transformers;

use App\Integrante;
use Flugg\Responder\Transformers\Transformer;

class IntegranteTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [
        'hogar' => HogarTransformer::class,
        'estadoNacimiento' => EstadoTransformer::class
    ];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param  \App\Integrante $integrante
     * @return array
     */
    public function transform(Integrante $integrante)
    {
        return [
            'id' => (int) $integrante->id,
            'apellido_paterno' => $integrante->apellido_paterno,
            'apellido_materno' => $integrante->apellido_materno,
            'nombre' => $integrante->nombre,
            'curp' => $integrante->curp,
            'fecha_nacimiento' => $integrante->fecha_nacimiento,
            'sexo' => $integrante->sexo,
            'hogar_id' => $integrante->hogar_id,
            'estado_nacimiento_id' => $integrante->estado_nacimiento_id,
        ];
    }
}
