<?php

namespace App;

use App\Transformers\IntegranteTransformer;
use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;

class Integrante extends Model implements Transformable
{
    protected $fillable = [

        "hogar_id"

    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;



    public function hogar()
    {
        return $this->belongsTo('App\Hogar');
    }

    public function estadoNacimiento()
    {
        return $this->belongsTo('App\Estado');
    }

    public function transformer()
    {
        // TODO: Implement transformer() method.
        return IntegranteTransformer::class;
    }
}
