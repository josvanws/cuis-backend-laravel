<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoMargen extends Model
{
        /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tipo_margenes';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function caminos() {
        return $this->hasMany('App\Camino');
    }
}
