<?php

namespace App\Http\Controllers;

use App\Integrante;
use Illuminate\Http\Request;

class IntegranteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return responder()->success(Integrante::all())->with('parentesco','estadoNacimiento')->respond();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $integrante = new Integrante;
        $integrante->apellido_paterno = $request->apellido_paterno;
        $integrante->apellido_materno = $request->apellido_materno;
        $integrante->nombre = $request->nombre;
        $integrante->parentesco_id = $request->parentesco_id;
        $integrante->curp = $request->curp;
        $integrante->fecha_nacimiento = $request->fecha_nacimiento;
        $integrante->sexo = $request->sexo;
        $integrante->hogar_id = $request->hogar_id;
        $integrante->estado_nacimiento_id = $request->estado_nacimiento_id;
        try{
            $integrante->save();
            return responder()->success()->respond();
        }catch (\Exception $ex){
            return responder()->error($ex->getCode(), $ex->getMessage())->respond();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Integrante  $integrante
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Integrante $integrante)
    {
        return responder()->success($integrante)->with('parentesco','estadoNacimiento')->respond();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Integrante  $integrante
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Integrante $integrante)
    {
        $integrante->apellido_paterno = $request->apellido_paterno;
        $integrante->apellido_materno = $request->apellido_materno;
        $integrante->nombre = $request->nombre;
        $integrante->parentesco_id = $request->parentesco_id;
        $integrante->curp = $request->curp;
        $integrante->fecha_nacimiento = $request->fecha_nacimiento;
        $integrante->sexo = $request->sexo;
        $integrante->hogar_id = $request->hogar_id;
        $integrante->estado_nacimiento_id = $request->estado_nacimiento_id;
        try{
            $integrante->save();
            return responder()->success()->respond();
        }catch (\Exception $ex){
            return responder()->error($ex->getCode(), $ex->getMessage())->respond();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Integrante $integrante
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Integrante $integrante)
    {
        $integrante->delete();
        return responder()->success()->respond();
    }
}
