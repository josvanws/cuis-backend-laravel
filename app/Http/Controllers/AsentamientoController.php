<?php

namespace App\Http\Controllers;

use App\Asentamiento;
use Illuminate\Http\Request;

class AsentamientoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return responder()->success(Asentamiento::all())->with('tipoAsentamiento')->respond();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $asentamiento = new Asentamiento;
        $asentamiento->tipo_asentamiento_id = $request->tipo_asentamiento_id;
        $asentamiento->nombre = $request->nombre;
        try{
            $asentamiento->save();
            return responder()->success()->respond();
        }catch (\Exception $ex){
            return responder()->error($ex->getCode(), $ex->getMessage())->respond();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Asentamiento  $asentamiento
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Asentamiento $asentamiento)
    {
        return responder()->success($asentamiento)->with('tipoAsentamiento')->respond();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Asentamiento  $asentamiento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Asentamiento $asentamiento)
    {
        $asentamiento->tipo_asentamiento_id = $request->tipo_asentamiento_id;
        $asentamiento->nombre = $request->nombre;
        try{
            $asentamiento->save();
            return responder()->success()->respond();
        }catch (\Exception $ex){
            return responder()->error($ex->getCode(), $ex->getMessage())->respond();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Asentamiento $asentamiento
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Asentamiento $asentamiento)
    {
        $asentamiento->delete();
        return responder()->success()->respond();
    }
}
