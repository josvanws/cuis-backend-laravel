<?php

namespace App\Http\Controllers;


use App\Http\Requests\LevantamientoRequest;
use Illuminate\Http\Request;
use App\Levantamiento;
use App\Integrante;
use App\Vialidad;
use App\Asentamiento;
use App\Direccion;
use App\Vivienda;
use App\Hogar;
use App\Camino;
use App\Carretera;
use Phaza\LaravelPostgis\Geometries\Point;
use GuzzleHttp\Client;
use App\Helpers\ValidaCurp;

class LevantamientoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param LevantamientoRequest $request
     * @return string
     */
    public function index(LevantamientoRequest $request)
    {
        $levantamiento = $request->validated();

        //Insertar a MongoDb
        Levantamiento::create( $levantamiento );

        $identificacion_geografica = $levantamiento["identificacion_geografica"];
        $identificacion_hogar = $levantamiento["identificacion_hogar"];

        // Insertar vialidad
        $vialidad = new Vialidad();
        $vialidad->tipo_vialidad_id = $identificacion_geografica["vialidad"]["tipo_vialidad"]["id"];
        $vialidad->nombre = $identificacion_geografica["vialidad"]["nombre_vialidad"];
        $vialidad->save();

        // Insertar asentamiento
        $asentamiento = new Asentamiento();
        $asentamiento->tipo_asentamiento_id = $identificacion_geografica["vialidad"]["tipo_asentamiento"]["id"];
        $asentamiento->nombre = $identificacion_geografica["vialidad"]["nombre_asentamiento"];
        $asentamiento->save();

        // Insertar entre Vialidad 1,2 y posterior
        $entre_vialidad1 = new Vialidad();
        $entre_vialidad1->tipo_vialidad_id = $identificacion_geografica["vialidad"]["entre_vialidad_uno"]["tipo_vialidad"]["id"];
        $entre_vialidad1->nombre = $identificacion_geografica["vialidad"]["entre_vialidad_uno"]["nombre_vialidad"];
        $entre_vialidad1->save();

        $entre_vialidad2 = new Vialidad();
        $entre_vialidad2->tipo_vialidad_id = $identificacion_geografica["vialidad"]["entre_vialidad_dos"]["tipo_vialidad"]["id"];
        $entre_vialidad2->nombre = $identificacion_geografica["vialidad"]["entre_vialidad_dos"]["nombre_vialidad"];
        $entre_vialidad2->save();

        $vialidad_posterior = new Vialidad();
        $vialidad_posterior->tipo_vialidad_id = $identificacion_geografica["vialidad"]["vialidad_posterior"]["tipo_vialidad"]["id"];
        $vialidad_posterior->nombre = $identificacion_geografica["vialidad"]["vialidad_posterior"]["nombre_vialidad"];
        $vialidad_posterior->save();

        // Obetner la última ubicación de las visitas.
        $longitude = end($levantamiento["visitas"])["fin"]["ubicacion"]["coordinates"][0];
        $latitude = end($levantamiento["visitas"])["fin"]["ubicacion"]["coordinates"][1];
        // buscar la implementacion

        // Insertar dirección
        $direccion = new Direccion();
        $direccion->localidad_id = $identificacion_geografica["localidad"]["clave"];
        $direccion->clave_ageb = $identificacion_geografica["ageb"];
        $direccion->clave_manzana = $identificacion_geografica["manzana"];
        $direccion->vialidad_id = $vialidad->id;
        $direccion->clave_manzana = $identificacion_geografica["manzana"];
        $direccion->num_exterior = $identificacion_geografica["vialidad"]["numero_exterior"]["numero"];
        $direccion->num_exterior_anterior = $identificacion_geografica["vialidad"]["numero_exterior_anterior"]["numero"];
        $direccion->letra = $identificacion_geografica["vialidad"]["numero_exterior"]["letra"];
        $direccion->num_interior = $identificacion_geografica["vialidad"]["numero_interior"]["numero"];
        $direccion->letra_interior = $identificacion_geografica["vialidad"]["numero_interior"]["letra"];
        $direccion->cp = $identificacion_geografica["vialidad"]["codigo_postal"];
        $direccion->asentamiento_id = $asentamiento->id;
        $direccion->entre_vialidad1_id = $entre_vialidad1->id;
        $direccion->entre_vialidad2_id = $entre_vialidad2->id;
        $direccion->vialidad_posterior_id = $vialidad_posterior->id;
        $direccion->referencias = $identificacion_geografica["vialidad"]["referencia"];
        $direccion->ubicacion = new Point($longitude, $latitude);
        $direccion->save();

        //Insertar Vivienda
        $vivienda = new Vivienda();
        $vivienda->direccion_id = $direccion->id;
        $vivienda->tipo_vivienda_id = $identificacion_hogar["tipo_vivienda"]["id"];
        $vivienda->numero_personas = $identificacion_hogar["habitantes"];
        $vivienda->numero_hogares = $identificacion_hogar["hogares"];
        $vivienda->save();

        //Insertar Hogar
        $hogar = new Hogar();
        $hogar->vivienda_id = $vivienda->id;
        $hogar->numero_integrantes = $identificacion_hogar["habitantes_hogar"];
        $hogar->save();

        //Insertar Camino
        $camino = new Camino();
        $camino->direccion_id = $direccion->id;
        $camino->tramo_origen = $identificacion_geografica["camino"]["tramo"]["origen"];
        $camino->tramo_destino = $identificacion_geografica["camino"]["tramo"]["destino"];
        $camino->tipo_margen_id = $identificacion_geografica["camino"]["margen"]["id"];
        $camino->km = $identificacion_geografica["camino"]["cadenamiento"]["kilometro"];
        $camino->metro = $identificacion_geografica["camino"]["cadenamiento"]["metro"];
        $camino->tipo_camino_id = $identificacion_geografica["camino"]["termino_generico"]["id"];
        $camino->save();

        // Insertar Carretera
        $carretera = new Carretera();
        $carretera->direccion_id = $direccion->id;
        $carretera->tipo_administracion_id = $identificacion_geografica["carretera"]["tipo_administracion"]["id"];
        $carretera->derecho_transito_id = $identificacion_geografica["carretera"]["derecho_transito"]["id"];
        $carretera->codigo = $identificacion_geografica["carretera"]["codigo"];
        $carretera->tramo_origen = $identificacion_geografica["carretera"]["tramo"]["origen"];
        $carretera->tramo_destino = $identificacion_geografica["carretera"]["tramo"]["destino"];
        $carretera->km = $identificacion_geografica["carretera"]["cadenamiento"]["kilometro"];
        $carretera->metro = $identificacion_geografica["carretera"]["cadenamiento"]["metro"];
        $carretera->save();

        foreach($levantamiento["integrantes"] as $integrante)
        {
            $integranteModel = new Integrante();
            $integranteModel->apellido_paterno = $integrante["apellido_paterno"];
            $integranteModel->apellido_materno = $integrante["apellido_materno"];
            $integranteModel->nombre = $integrante["nombres"];
            $integranteModel->curp = $integrante["curp"];
            $integranteModel->sexo = $integrante["genero"]["id"];
            $integranteModel->fecha_nacimiento = $integrante["fecha_nacimiento"];
            $integranteModel->hogar_id = $hogar->id;
            $integranteModel->estado_nacimiento_id = $integrante["lugar_nacimiento"]["id"];

           $integranteModel->save();
        }

       return responder()->success($request)->respond();
    }
}
