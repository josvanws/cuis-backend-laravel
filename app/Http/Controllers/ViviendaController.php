<?php

namespace App\Http\Controllers;

use App\Vivienda;
use Illuminate\Http\Request;

class ViviendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return responder()->success(Vivienda::all())->with('direccion','tipoVivienda')->respond();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $vivienda = new Vivienda;
        $vivienda->direccion_id = $request->direccion_id;
        $vivienda->tipo_vivienda_id = $request->tipo_vivienda_id;
        $vivienda->numero_personas = $request->numero_personas;
        $vivienda->numero_hogares = $request->numero_hogares;
        try{
            $vivienda->save();
            return responder()->success()->respond();
        }catch (\Exception $ex){
            return responder()->error($ex->getCode(), $ex->getMessage())->respond();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vivienda  $vivienda
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Vivienda $vivienda)
    {
        return responder()->success($vivienda)->with('direccion','tipoVivienda')->respond();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vivienda  $vivienda
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Vivienda $vivienda)
    {
        $vivienda->direccion_id = $request->direccion_id;
        $vivienda->tipo_vivienda_id = $request->tipo_vivienda_id;
        $vivienda->numero_personas = $request->numero_personas;
        $vivienda->numero_hogares = $request->numero_hogares;
        try{
            $vivienda->save();
            return responder()->success()->respond();
        }catch (\Exception $ex){
            return responder()->error($ex->getCode(), $ex->getMessage())->respond();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Vivienda $vivienda
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Vivienda $vivienda)
    {
        $vivienda->delete();
        return responder()->success()->respond();
    }
}
