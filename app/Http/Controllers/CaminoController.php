<?php

namespace App\Http\Controllers;

use App\Camino;
use App\Transformers\CaminoTransformer;
use Illuminate\Http\Request;

class CaminoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return responder()->success(Camino::all())->respond();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $camino = new Camino;
        $camino->direccion_id = $request->direccion_id;
        $camino->tramo_origen = $request->tramo_origen;
        $camino->tramo_destino = $request->tramo_destino;
        $camino->tipo_margen_id = $request->tipo_margen_id;
        $camino->km = $request->km;
        $camino->metro = $request->metro;
        $camino->tipo_camino_id = $request->tipo_camino_id;
        try{
            $camino->save();
            return responder()->success()->respond();
        }catch (\Exception $ex){
            return responder()->error($ex->getCode(), $ex->getMessage())->respond();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Camino  $camino
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Camino $camino)
    {
        return responder()->success($camino)->with('localidad')->respond();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Camino  $camino
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Camino $camino)
    {
        $camino->direccion_id = $request->direccion_id;
        $camino->tramo_origen = $request->tramo_origen;
        $camino->tramo_destino = $request->tramo_destino;
        $camino->tipo_margen_id = $request->tipo_margen_id;
        $camino->km = $request->km;
        $camino->metro = $request->metro;
        $camino->tipo_camino_id = $request->tipo_camino_id;
        try{
            $camino->save();
            return responder()->success()->respond();
        }catch (\Exception $ex){
            return responder()->error($ex->getCode(), $ex->getMessage())->respond();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Camino $camino
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Camino $camino)
    {
        $camino->delete();
        return responder()->success()->respond();
    }
}
