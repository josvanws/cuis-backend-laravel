<?php

namespace App\Http\Controllers;

use App\Hogar;
use Illuminate\Http\Request;

class HogarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return responder()->success(Hogar::all())->respond();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $hogar = new Hogar;
        $hogar->vivienda_id = $request->vivienda_id;
        $hogar->numero_integrantes = $request->numero_integrantes;
        try{
            $hogar->save();
            return responder()->success()->respond();
        }catch (\Exception $ex){
            return responder()->error($ex->getCode(), $ex->getMessage())->respond();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Hogar  $hogar
     * @return \Illuminate\Http\Response
     */
    public function show(Hogar $hogar)
    {
        return responder()->success($hogar)->respond();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hogar  $hogar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hogar $hogar)
    {
        $hogar->vivienda_id = $request->vivienda_id;
        $hogar->numero_integrantes = $request->numero_integrantes;
        try{
            $hogar->save();
            return responder()->success()->respond();
        }catch (\Exception $ex){
            return responder()->error($ex->getCode(), $ex->getMessage())->respond();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Hogar $hogar
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Hogar $hogar)
    {
        $hogar->delete();
        return responder()->success()->respond();
    }
}
