<?php

namespace App\Http\Controllers;

use App\Vialidad;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class VialidadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return responder()->success(Vialidad::all())->respond();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $vialidad = new Vialidad;
        $vialidad->tipo_vialidad_id = $request->tipo_vialidad_id;
        $vialidad->nombre = $request->nombre;
        try{
            $vialidad->save();
            return responder()->success()->respond();
        }catch (\Exception $ex){
            return responder()->error($ex->getCode(), $ex->getMessage())->respond();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Vialidad $vialidad
     * @return Response
     */
    public function show(Vialidad $vialidad)
    {
        return responder()->success($vialidad)->respond();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Vialidad $vialidad
     * @return Response
     */
    public function update(Request $request, Vialidad $vialidad)
    {
        $vialidad->tipo_vialidad_id = $request->tipo_vialidad_id;
        $vialidad->nombre = $request->nombre;
        try{
            $vialidad->save();
            return responder()->success()->respond();
        }catch (\Exception $ex){
            return responder()->error($ex->getCode(), $ex->getMessage())->respond();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Vialidad $vialidad
     * @return Response
     * @throws Exception
     */
    public function destroy(Vialidad $vialidad)
    {
        $vialidad->delete();
        return responder()->success()->respond();
    }
}
