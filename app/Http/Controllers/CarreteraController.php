<?php

namespace App\Http\Controllers;

use App\Carretera;
use Illuminate\Http\Request;

class CarreteraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return responder()->success(Carretera::all())->respond();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $carretera = new Carretera;
        $carretera->direccion_id = $request->direccion_id;
        $carretera->tipo_administracion_id = $request->tipo_administracion_id;
        $carretera->derecho_transito_id = $request->derecho_transito_id;
        $carretera->codigo = $request->codigo;
        $carretera->tramo_origen = $request->tramo_origen;
        $carretera->tramo_destino = $request->tramo_destino;
        $carretera->km = $request->km;
        $carretera->metro = $request->metro;
        $carretera->save();
        try{
            $carretera->save();
            return responder()->success()->respond();
        }catch (\Exception $ex){
            return responder()->error($ex->getCode(), $ex->getMessage())->respond();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Carretera  $carretera
     * @return \Illuminate\Http\Response
     */
    public function show(Carretera $carretera)
    {
        return responder()->success($carretera)->respond();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Carretera  $carretera
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Carretera $carretera)
    {
        $carretera->direccion_id = $request->direccion_id;
        $carretera->tipo_administracion_id = $request->tipo_administracion_id;
        $carretera->derecho_transito_id = $request->derecho_transito_id;
        $carretera->codigo = $request->codigo;
        $carretera->tramo_origen = $request->tramo_origen;
        $carretera->tramo_destino = $request->tramo_destino;
        $carretera->km = $request->km;
        $carretera->metro = $request->metro;
        try{
            $carretera->save();
            return responder()->success()->respond();
        }catch (\Exception $ex){
            return responder()->error($ex->getCode(), $ex->getMessage())->respond();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Carretera $carretera
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Carretera $carretera)
    {
        $carretera->delete();
        return responder()->success()->respond();
    }
}
