<?php

namespace App\Http\Controllers;

use App\Direccion;
use App\Exceptions\SoldOutException;
use App\Municipio;
use App\Transformers\DireccionTransformer;
use Flugg\Responder\Serializers\NoopSerializer;
use Flugg\Responder\TransformBuilder;
use Illuminate\Http\Request;
use League\Fractal\Serializer\JsonApiSerializer;
use Phaza\LaravelPostgis\Geometries\Point;

class DireccionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
//        throw new SoldOutException();
//        return responder()->success(Direccion::paginate())->with('localidad')->only([
//            'direcciones' =>['referencias', 'cp'],
//            'localidad' => ['nombre']
//        ]);
//        return responder()->success(Direccion::paginate())->with('localidad')->respond();
//        return $transformation->resource(Direccion::all())->with('localidad')->transform();
//        return $transformation->resource(Direccion::all())->serializer(NoopSerializer::class)->transform();
//        return responder()->error('sold_out_error')->data(['municipios'=> Municipio::all()])->respond();
        return responder()->success(Direccion::all())->with('localidad')->respond();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $direccion = new Direccion;
        $direccion->localidad_id = $request->localidad_id;
        $direccion->clave_ageb = $request->clave_ageb;
        $direccion->clave_manzana = $request->clave_manzana;
        $direccion->vialidad_id = $request->vialidad_id;
        $direccion->num_exterior = $request->num_exterior;
        $direccion->letra = $request->letra;
        $direccion->num_exterior_anterior = $request->num_exterior_anterior;
        $direccion->num_interior = $request->num_interior;
        $direccion->letra_interior = $request->letra_interior;
        $direccion->cp = $request->cp;
        $direccion->asentamiento_id = $request->asentamiento_id;
        $direccion->entre_vialidad1_id = $request->entre_vialidad1_id;
        $direccion->entre_vialidad2_id = $request->entre_vialidad2_id;
        $direccion->vialidad_posterior_id = $request->vialidad_posterior_id;
        $direccion->referencias = $request->referencias;
        $direccion->ubicacion = new Point($request->ubicacion["coordinates"][1], $request->ubicacion["coordinates"][0]);
        try{
            $direccion->save();
            return responder()->success()->respond();
        }catch (\Exception $ex){
            return responder()->error($ex->getCode(), $ex->getMessage())->respond();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Direccion  $direccion
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Direccion $direccion)
    {
        return responder()->success($direccion)->with('localidad')->respond();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Direccion  $direccion
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Direccion $direccion)
    {
        $direccion->localidad_id = $request->localidad_id;
        $direccion->clave_ageb = $request->clave_ageb;
        $direccion->clave_manzana = $request->clave_manzana;
        $direccion->vialidad_id = $request->vialidad_id;
        $direccion->num_exterior = $request->num_exterior;
        $direccion->letra = $request->letra;
        $direccion->num_exterior_anterior = $request->num_exterior_anterior;
        $direccion->num_interior = $request->num_interior;
        $direccion->letra_interior = $request->letra_interior;
        $direccion->cp = $request->cp;
        $direccion->asentamiento_id = $request->asentamiento_id;
        $direccion->entre_vialidad1_id = $request->entre_vialidad1_id;
        $direccion->entre_vialidad2_id = $request->entre_vialidad2_id;
        $direccion->vialidad_posterior_id = $request->vialidad_posterior_id;
        $direccion->referencias = $request->referencias;
        $direccion->ubicacion = new Point($request->ubicacion["coordinates"][0], $request->ubicacion["coordinates"][1]);
        try{
            $direccion->save();
            return responder()->success()->respond();
        }catch (\Exception $ex){
            return responder()->error($ex->getCode(), $ex->getMessage())->respond();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Direccion $direccion
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Direccion $direccion)
    {
        $direccion->delete();
        return responder()->success()->respond();
    }
}
