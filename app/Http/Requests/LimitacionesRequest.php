<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LimitacionesRequest extends FormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $catalogo_clave_rules = (new CatalogoSimpleClaveRequest())->rules();
        $catalogo_clave = \Gealtec::iter_rules($catalogo_clave_rules, ["limitacion"]);

        $catalogo_simple_rules = (new CatalogoSimpleRequest())->rules();
        $catalogo_simple = \Gealtec::iter_rules($catalogo_simple_rules, ["grado", "origen"]);

        return array_merge($catalogo_clave, $catalogo_simple);
    }
}
