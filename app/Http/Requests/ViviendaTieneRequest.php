<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ViviendaTieneRequest extends FormRequest
{
     /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "area" => ["required_if:datos_hogar.vivienda_tiene.concepto.clave,filled", "digits_between:1,5"]
        ];

        $catalogo_clave_rules = (new CatalogoSimpleClaveRequest())->rules("required_if:datos_hogar.vivienda_tiene.concepto,present");
        $campos_clave_simple = \Gealtec::iter_rules($catalogo_clave_rules, ["concepto"]);

        return array_merge(
            $rules,
            $campos_clave_simple
        );
    }
}
