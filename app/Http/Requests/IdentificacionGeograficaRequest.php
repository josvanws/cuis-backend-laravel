<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IdentificacionGeograficaRequest extends FormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "ageb" => ["required", "alpha_num"],
            "manzana" => ["numeric","digits_between:1,3"]
        ];

        $catalogo_gis_rules = (new CatalogoGisRequest())->rules();
        $catalogo_gis = \Gealtec::iter_rules($catalogo_gis_rules, ["entidad", "municipio", "localidad"]);

        // Se podría cambiar el campo: tipo_domicilio por referencia_vivienda
        $tipo_domicilio_rules = (new CatalogoSimpleRequest())->rules();
        $tipo_domicilio = \Gealtec::iter_rules($tipo_domicilio_rules, ["tipo_domicilio"]);

        $carretera_rules = (new CarreteraRequest())->rules();
        $carretera = \Gealtec::iter_rules($carretera_rules, ["carretera"]);

        $camino_rules = (new CaminoRequest())->rules();
        $camino = \Gealtec::iter_rules($camino_rules, ["camino"]);

        $vialidad_rules = (new VialidadRequest())->rules();
        $vialidad = \Gealtec::iter_rules($vialidad_rules, ["vialidad"]);

        return array_merge(
            $catalogo_gis,
            $rules,
            $tipo_domicilio,
            $carretera,
            $camino,
            $vialidad
        );


    }
}
