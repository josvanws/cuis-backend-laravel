<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IdentificacionHogarRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "habitantes" => ["required", "digits_between:1,2"],
            "hogares" => ["required", "digits:1"],
            "habitantes_hogar" => ["required", "digits_between:1,2"],
            "comparten_gastos" => ["required", "boolean"],
            "habitanVivienda" => ["required", "boolean"],
            "telefono" => ["required_if:identificacion_hogar.tiene_telefono.id,1", "digits:10"],
        ];

        $informante_rules = (new InformanteRequest())->rules();
        $informante = \Gealtec::iter_rules($informante_rules, ["informante"]);

        $catalogo_simple_rules = (new CatalogoSimpleRequest())->rules();
        $catalogo_simple = \Gealtec::iter_rules($catalogo_simple_rules, ["tipo_vivienda", "tiene_telefono"]);

        return array_merge(
            $rules,
            $catalogo_simple,
            $informante
        );
    }
}
