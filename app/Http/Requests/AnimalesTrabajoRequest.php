<?php

namespace App\Http\Requests;

class AnimalesTrabajoRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "cantidad" => ["nullable","numeric", "digits_between:1,5"]
        ];

        $catalogo_clave_rules = (new CatalogoSimpleClaveRequest())->rules();
        $animal = \Gealtec::iter_rules($catalogo_clave_rules, ["animal"]);

        return array_merge(
            $rules,
            $animal
        );

    }
}
