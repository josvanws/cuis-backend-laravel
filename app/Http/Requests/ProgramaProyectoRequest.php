<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProgramaProyectoRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "folio_cuis" => ["required", "numeric"],
            "tipo" => ["nullable"],
            "folio" => ["required", "alpha_num"]
        ];
    }
}
