<?php

namespace App\Http\Requests;

use App\Rules\Curp;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class IntegrantesRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $estado_civil_rule = "required_if:integrantes.*.estado_civil.id,1,2";
        $rules = [
            "nombres" => ["required", "max:30"],
            "apellido_paterno" => ["required", "max:30"],
            "apellido_materno" => ["required", "max:30"],
            "es_informante" => ["nullable", "boolean"],
            "curp" => ["nullable","size:18"],
            "fecha_nacimiento" => ["nullable", "date"],
            "edad" => ["nullable",  "digits_between:1,2", "lte:98"],
            "anio_residencia" => ["nullable", "digits:4", "lte:" . date("Y")],
            "padre_index" => [""],
            "madre_index" => [""],
            "habla_espaniol" => ["nullable", "boolean"],
            "asiste_escuela" => ["nullable", "boolean"],
            "conyuge_index" => [$estado_civil_rule, "nullable"],
            "subordinado" => ["nullable", "boolean"],
            "independiente" => ["required_if:integrantes.*.subordinado,false","boolean"],
            "recibio_pago" => ["nullable", "boolean"],
            "salario" => ["required_if:integrantes.*.recibio_pago,true", "digits_between:1,5"],
            "jubilado_pais" => ["required_if:integrantes.0.jubilado.id,1", "boolean"],
            "jubilado_extranjero" => ["required_if:integrantes.0.jubilado.id,1","boolean"],
            "otro_proyectos_productivos_desc" => ["required_if:integrantes.0.proyectos_productivos.0.id,11", "max:100"]


        ];

        $catalogo_simple_rules = (new CatalogoSimpleRequest())->rules("nullable");
        $catalogo_clave_rules = (new CatalogoSimpleClaveRequest())->rules("nullable");

        $catalogo_simple = \Gealtec::iter_rules($catalogo_simple_rules, [
            "condicion_residencia",
            "parentesco",
            "tiene_curp",
            "lugar_nacimiento",
            "acta_nacimiento",
            "vive_padre_hogar",
            "vive_madre_hogar",
            "habla_lengua_indigena",
            "lengua_indigena",
            "considera_indigena",
            "alfabeta",
            "estado_civil",
            "condicion_actividad",
            "ocupacion",
            "tiempo_trabajo",
            "no_trabaja_anio",
            "jubilado",
            "inapam",
            "genero"
        ]);

        $catalogo_clave_array = \Gealtec::iter_rules($catalogo_clave_rules, [
            "prestaciones_laborales",
            "seguros_voluntarios",
            "recibe_dinero_por",
            "enfermedades"
        ], "*");

        $asiste_escuela_rule = "required_if:integrantes.*.asiste_escuela,false";
        $catalogo_simple_rule_reqif_asiste_escuela = (new CatalogoSimpleRequest())->rules($asiste_escuela_rule);
        $catsimp_rule_reqif_asiste_escuela = \Gealtec::iter_rules($catalogo_simple_rule_reqif_asiste_escuela, ["abandono_escolar"]);

        $catsim_conyuge_hogar_rule = (new CatalogoSimpleRequest())->rules($estado_civil_rule);
        $conyuge_hogar = \Gealtec::iter_rules($catsim_conyuge_hogar_rule, ["conyuge_hogar"]);

        $actividad_mes_anterior_rule = "required_if:integrantes.*.condicion_actividad.id,4,5,6,7";
        $catsim_actividad_mes_anterior_rule = (new CatalogoSimpleRequest())->rules($actividad_mes_anterior_rule);
        $actividad_mes_anterior = \Gealtec::iter_rules($catsim_actividad_mes_anterior_rule, ["actividad_mes_anterior"]);

        $no_trabajo_mes_anterior_rule = "required_if:integrantes.*.actividad_mes_anterior.id,5,6";
        $catsim_no_trabajo_mes_anterior_rule = (new CatalogoSimpleRequest())->rules($no_trabajo_mes_anterior_rule);
        $no_trabajo_mes_anterior = \Gealtec::iter_rules($catsim_no_trabajo_mes_anterior_rule, ["no_trabajo_mes_anterior"]);

        $no_trabaja_anio_rule = "required_if:integrantes.*.tiempo_trabajo.id,1";
        $catsim_no_trabaja_anio_rule = (new CatalogoSimpleRequest())->rules($no_trabaja_anio_rule);
        $no_trabaja_anio = \Gealtec::iter_rules($catsim_no_trabaja_anio_rule, ["no_trabaja_anio"]);

        $periodo_salario_rule = "required_if:integrantes.*.salario,98000";
        $catsim_periodo_salario_rule = (new CatalogoSimpleRequest())->rules($periodo_salario_rule);
        $periodo_salario = \Gealtec::iter_rules($catsim_periodo_salario_rule, ["periodo_salario"]);

        $arreglo_catalogo_simple = \Gealtec::iter_rules($catalogo_simple_rules, [
            "derechohabiencia",
            "motivos_derechohabiencia",
            "proyectos_productivos"
        ], "*");

        $escolaridad_rules = (new EscolaridadRequest())->rules();
        $escolaridad = \Gealtec::iter_rules($escolaridad_rules, ["escolaridad"]);

        $limitaciones_rules = (new LimitacionesRequest())->rules();
        $limitaciones = \Gealtec::iter_rules($limitaciones_rules, ["limitaciones"], "*");

        $otro_ingreso_rules = (new OtroIngresoRequest())->rules();
        $otro_ingreso = \Gealtec::iter_rules($otro_ingreso_rules, ["otro_ingreso"]);

        return array_merge(
            $rules,
            $catalogo_simple,
            $catalogo_clave_array,
            $arreglo_catalogo_simple,
            $escolaridad,
            $limitaciones,
            $otro_ingreso,
            $catsimp_rule_reqif_asiste_escuela,
            $conyuge_hogar,
            $actividad_mes_anterior,
            $no_trabajo_mes_anterior,
            $no_trabaja_anio
        );
    }
}
