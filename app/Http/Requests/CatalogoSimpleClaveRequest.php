<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CatalogoSimpleClaveRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @param string|callable $required_if
     * @return array
     */
    public function rules($required_if = null)
    {
        return [
            "clave" => [$required_if ?: "required", "alpha"],
            "nombre" => [$required_if ?: "required"]
        ];
    }
}
