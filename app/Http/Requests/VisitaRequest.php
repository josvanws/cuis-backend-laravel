<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VisitaRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $georreferencia_rules = (new GeorreferenciaRequest())->rules();

        return \Gealtec::iter_rules($georreferencia_rules, ["inicio", "fin"]);
    }
}
