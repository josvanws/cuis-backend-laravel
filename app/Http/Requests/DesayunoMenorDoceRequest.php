<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DesayunoMenorDoceRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $catalogo_simple_rules = (new CatalogoSimpleRequest())->rules();
        $campos_catalogo_simple = \Gealtec::iter_rules($catalogo_simple_rules, ["desayuna", "motivo"]);
        return $campos_catalogo_simple
        ;
    }
}
