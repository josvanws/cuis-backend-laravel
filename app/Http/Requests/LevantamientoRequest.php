<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;

class LevantamientoRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            "encuesta_por_traductor" => ["required", "boolean"],
            "observaciones" => ["nullable", "max:100"]
        ];

        $programa_proyecto_rules = (new ProgramaProyectoRequest())->rules();
        $programa_proyecto = \Gealtec::iter_rules($programa_proyecto_rules, ["programa_proyecto"]);

        $visita_rules = (new VisitaRequest())->rules();
        $visita = \Gealtec::iter_rules($visita_rules, ["visitas"], "*");

        $encuestador_rules = (new EncuestadorRequest())->rules();
        $encuestador = \Gealtec::iter_rules($encuestador_rules, ["encuestador"]);


        $catalogos_simples_rules = (new CatalogoSimpleRequest())->rules();
        $catalogos_simples = \Gealtec::iter_rules($catalogos_simples_rules,
            ["tipo_proceso", "punto_recoleccion", "codigo_resultado"]);

        $identificacion_geografica_rules = (new IdentificacionGeograficaRequest())->rules();
        $identificacion_geografica = \Gealtec::iter_rules($identificacion_geografica_rules, ["identificacion_geografica"]);

        $identificacion_hogar_rules = (new IdentificacionHogarRequest())->rules();
        $identificacion_hogar = \Gealtec::iter_rules($identificacion_hogar_rules, ["identificacion_hogar"]);

        $integrantes_rules = (new IntegrantesRequest())->rules();
        $integrantes = \Gealtec::iter_rules($integrantes_rules, ["integrantes"], "*");

        $datosHogar_rules = (new DatosHogarRequest())->rules();
        $datosHogar = \Gealtec::iter_rules($datosHogar_rules, ["datos_hogar"]);



        return array_merge(
            $rules,
            $programa_proyecto,
            $visita,
            $encuestador,
            $catalogos_simples,
            $identificacion_geografica,
            $identificacion_hogar,
            $integrantes,
            $datosHogar
        );
    }

    public function withValidator(Validator $validator)
    {
        $integrantes = @$validator->getData()["integrantes"] ?: [];
        $simple_catalog_rules = (new CatalogoSimpleRequest())->rules();

        foreach ($simple_catalog_rules as $key => $rules) {
            if (in_array("required", $rules)) {
                foreach ($integrantes as $index => $integrante) {
                    $validator->someTimes("integrantes.{$index}.periodo_salario.{$key}", "required", function ($input) use ($index) {
                        $salario = @$input->integrantes[$index]["salario"] ?: 0;
                        return $salario <= 98000;
                    });
                }
            }
        }
    }
}
