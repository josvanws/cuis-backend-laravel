<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TipoEscrituraRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "integrantes_con_escritura.*" => ["nullable", "digits_between:1,2"]
        ];

        $catalogo_simple_rules = (new CatalogoSimpleRequest())->rules();
        $campos_catalogo_simple = \Gealtec::iter_rules($catalogo_simple_rules, ["tipo_escritura"]);

        return array_merge(
            $rules,
            $campos_catalogo_simple
        );
    }
}
