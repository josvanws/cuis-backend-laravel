<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InformanteRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "folio_identificacion" => ["nullable", "alpha_num", "max:20"],
            "folio_documento_edad" => ["nullable", "alpha_num", "max:20"],
            "firma_informante" => ["required"]
        ];

        $catalogo_simple_rules = (new CatalogoSimpleRequest())->rules("nullable");
        $catalogo_simple = \Gealtec::iter_rules($catalogo_simple_rules, ["identificacion", "tipo_documento_edad"]);

        return array_merge(
            $rules,
            $catalogo_simple
        );
    }
}
