<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TieneSirveRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "sirve" => ["required", "boolean"]
        ];

        $catalogo_clave_rules = (new CatalogoSimpleClaveRequest())->rules();
        $concepto = \Gealtec::iter_rules($catalogo_clave_rules, ["concepto"]);

        return array_merge(
            $rules,
            $concepto
        );
    }
}
