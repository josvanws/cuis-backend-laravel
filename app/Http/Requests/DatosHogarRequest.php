<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DatosHogarRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "remesas" => "boolean",
            "comidas_dia" => ["nullable", "digits_between:1,2"],
            "cuartos" => ["nullable", "digits_between:1,2"],
            "cuartos_dormir" => ["nullable", "digits_between:1,2"],
            "piso_por_programa" => ["required_if:datos_hogar.material_piso.id,2", "boolean"],
            "piso_agrietado" => ["required_if:datos_hogar.material_piso.id,3", "boolean"],
            "piso_tierra" => ["required_if:datos_hogar.material_piso.id,1", "boolean"],
            "techo_riesgo" => ["required_if:datos_hogar.material_techo.id,3,4,5,6,7,8,9", "boolean"],
            "pared_agrietado" => ["required_if:datos_hogar.material_pared.id,6,7,8", "boolean"],
            "banio_por_programa" => ["required_if:datos_hogar.tipo_banio.id,1", "boolean"],
            "banio_exclusivo" => ["required_if:datos_hogar.tipo_banio.id,2,3,4", "boolean"],
            "otro_tratamiento_desc" => ["nullable", "max:50"],
            "hidroponia" => ["nullable", "boolean"]

        ];

        $catalogo_simple_rules = (new CatalogoSimpleRequest())->rules("nullable");
        $catalogo_clave_rules = (new CatalogoSimpleClaveRequest())->rules();

        $catalogos_simples = \Gealtec::iter_rules($catalogo_simple_rules, [
            "tiene_gasto",
            "material_piso",
            "material_techo",
            "tipo_banio",
            "tipo_agua",
            "tipo_drenaje",
            "basura_accion",
            "combustible_cocina",
            "tipo_luz",
            "tipo_vivienda",
            "tierra_cultivo",
            "tiene_proyectos_productivos"
        ]);

        $material_pared_rule = "required_if:datos_hogar.material_techo.id,1,2";
        $catsim_material_pared_rules = (new CatalogoSimpleRequest())->rules($material_pared_rule);
        $material_pared = \Gealtec::iter_rules($catsim_material_pared_rules, ["material_pared"]);

        $array_catalogos_simples = \Gealtec::iter_rules($catalogo_simple_rules, [
            "lugar_atencion_medica",
            "productos_cultivo"
        ], "*");

        $trabajo_no_remunerado_rules = (new TrabajoNoRemuneradoRequest())->rules();
        $trabajo_no_remunerado = \Gealtec::iter_rules($trabajo_no_remunerado_rules, ["trabajo_no_remunerado"], "*");

        $gastos_rules = (new GastosRequest())->rules();
        $gastos = \Gealtec::iter_rules($gastos_rules, ["gastos"], "*");

        $comidas_semana_rules = (new ComidasSemanaRequest())->rules();
        $comida_semana = \Gealtec::iter_rules($comidas_semana_rules, ["comidas_semana"], "*");

        $campos_arreglos_clave_nombre = \Gealtec::iter_rules($catalogo_clave_rules, [
            "alimentacion_mayor_edad",
            "alimentacion_menor_edad",
            "tratamiento_agua",
            "forma_cultivo"
        ], "*");

        $desayuno_menor_doce_rules = (new DesayunoMenorDoceRequest())->rules();
        $desayuno_menor_doce = \Gealtec::iter_rules($desayuno_menor_doce_rules, ["desayuno_menor_doce"]);

        $aparato_cocinar_rules = (new AparatoCocinar())->rules("required_if:datos_hogar.combustible_cocina.id,5");
        $aparato_cocinar = \Gealtec::iter_rules($aparato_cocinar_rules, ["aparato_cocinar"], "*");

       
        $tiene_sirve_rules = (new TieneSirveRequest())->rules();
        $tiene_sirve = \Gealtec::iter_rules($tiene_sirve_rules, ["tiene_sirve"], "*");

        $tipo_escritura_rules = (new TipoEscrituraRequest())->rules();
        $tipo_escritura = \Gealtec::iter_rules($tipo_escritura_rules, ["tipo_escritura"]);

        $vivienda_tiene_rules = (new ViviendaTieneRequest())->rules();
        $vivienda_tiene = \Gealtec::iter_rules($vivienda_tiene_rules, ["vivienda_tiene"]);

        $tenencia_tierra_cultivo_rules = (new TenenciaTierraCultivoRequest())->rules();
        $tenencia_tierra_cultivo = \Gealtec::iter_rules($tenencia_tierra_cultivo_rules, ["tenencia_tierra_cultivo"]);

        $animales_trabajo_rules = (new AnimalesTrabajoRequest())->rules();
        $animales_trabajo = \Gealtec::iter_rules($animales_trabajo_rules, ["animales_trabajo_consumos"], "*");


        return array_merge(
            $rules,
            $catalogos_simples,
            $array_catalogos_simples,
            $trabajo_no_remunerado,
            $gastos,
            $comida_semana,
            $campos_arreglos_clave_nombre,
            $desayuno_menor_doce,
            $aparato_cocinar,
            $tiene_sirve,
            $tipo_escritura,
            $vivienda_tiene,
            $tenencia_tierra_cultivo,
            $animales_trabajo,
            $material_pared

        );
    }
}
