<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarreteraRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
            $tipo_domicilio_rule = "required_if:identificacion_geografica.tipo_domicilio.id,1";
        $rules = [
            "codigo" => [$tipo_domicilio_rule, "digits_between:1,3"],
            "tramo.origen" => ["nullable", "max:30"],
            "tramo.destino" => ["nullable", "max:30"],
            "cadenamiento.kilometro" => ["digits_between:1,4"],
            "cadenamiento.metro" => ["digits_between:1,3"]
        ];

        $catalogos_simples_rules = (new CatalogoSimpleRequest())->rules($tipo_domicilio_rule);
        $catalogos_simples = \Gealtec::iter_rules($catalogos_simples_rules, ["tipo_administracion","derecho_transito"]);

        return array_merge(
            $rules,
            $catalogos_simples
        );
    }

    public function messages()
    {
        return [
            "cadenamiento.kilometro" => "Debe ser numérico.",
            "cadenamiento.metro" => "El dato debe ser numérico."
        ];
    }
}
