<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EncuestadorRequest extends FormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "clave" => ["required", "alpha_num", "max:20"],
            "nombres" => ["required", "max:60"],
            "apellidos" => ["required","max:60"],
            "firma" => []
        ];
    }
}
