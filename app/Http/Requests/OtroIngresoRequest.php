<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OtroIngresoRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
          "salario.*" => ["required", "numeric"]
        ];

        $catalogo_clave_rules = (new CatalogoSimpleClaveRequest())->rules();
        $catalogo_clave = \Gealtec::iter_rules($catalogo_clave_rules, ["ingreso"], "*");

        return array_merge(
            $rules,
            $catalogo_clave
        );
    }
}
