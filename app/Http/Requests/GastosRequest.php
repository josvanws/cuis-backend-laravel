<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GastosRequest extends FormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "monto" => "numeric"
        ];

        $clave_nombre_rules = (new CatalogoSimpleClaveRequest())->rules();
        $campos_clave_nombre = \Gealtec::iter_rules($clave_nombre_rules, ["gasto"]);
        return array_merge(
            $rules,
            $campos_clave_nombre
        );
    }
}
