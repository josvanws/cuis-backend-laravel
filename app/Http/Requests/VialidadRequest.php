<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VialidadRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $tipo_domicilio_rule = "required_if:identificacion_geografica.tipo_domicilio.id,3";
        $rules = [
            "nombre_vialidad" => [$tipo_domicilio_rule,"max:60"],
            "numero_exterior.numero" => ["nullable", "digits_between:1,4"],
            "numero_exterior.letra" => ["nullable", "alpha_num","max:2"],
            "numero_exterior_anterior.numero" => ["nullable", "digits_between:1,4"],
            "numero_exterior_anterior.letra" => ["nullable", "alpha_num","max:2"],
            "numero_interior.numero" => ["nullable", "digits_between:1,4"],
            "numero_interior.letra" => ["nullable", "alpha_num","max:2"],
            "codigo_postal" => ["nullable", "digits:5"],
            "nombre_asentamiento" => ["nullable","max:60"],
            "referencia" => ["nullable", "max:100"]
        ];

        $catalogo_simple_rules = (new CatalogoSimpleRequest())->rules($tipo_domicilio_rule);
        $catalogo_simple = \Gealtec::iter_rules($catalogo_simple_rules, ["tipo_vialidad"]);
        $catalogo_simple2 = \Gealtec::iter_rules($catalogo_simple_rules, ["tipo_vialidad", "tipo_asentamiento"]);

        $entre_vialidad_rules = (new EntreVialidadRequest())->rules();
        $entre_vialidad = \Gealtec::iter_rules($entre_vialidad_rules, [
            "entre_vialidad_uno",
            "entre_vialidad_dos",
            "vialidad_posterior"
        ]);

        return array_merge (
            $rules,
            $catalogo_simple,
            $catalogo_simple2,
            $entre_vialidad
        );
    }
}
