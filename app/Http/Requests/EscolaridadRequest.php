<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EscolaridadRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $catalogo_simple_rules = (new CatalogoSimpleRequest())->rules();
        $catalogo_simple = \Gealtec::iter_rules($catalogo_simple_rules, ["nivel", "grado"]);
        return $catalogo_simple;
    }
}
