<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ComidasSemanaRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [

        ];
        $catalogo_simple_rules = (new CatalogoSimpleRequest())->rules();
        $catalogo_clave_rules = (new CatalogoSimpleClaveRequest())->rules();

        $catalogos_simples = \Gealtec::iter_rules($catalogo_simple_rules, ["frecuencia"]);
        $catalogos_clave = \Gealtec::iter_rules($catalogo_clave_rules, ["tipo_comida"]);
        return array_merge(
            $rules,
            $catalogos_simples,
            $catalogos_clave
        );
    }
}
