<?php

namespace App\Http\Requests;

class AparatoCocinar extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules($required_if = null)
    {
        $rules = [
            "dentro_vivienda" => [$required_if ?: "nullable","boolean"]
        ];

        $catalogo_simple_rules = (new CatalogoSimpleRequest())->rules($required_if);
        $campos_catalogo_simple = \Gealtec::iter_rules($catalogo_simple_rules, ["aparato"]);


        return array_merge(
            $rules,
            $campos_catalogo_simple
        );
    }
}
