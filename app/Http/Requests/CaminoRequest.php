<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CaminoRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $tipo_domicilio_rule = "required_if:identificacion_geografica.tipo_domicilio.id,2";
        $rules = [
            "tramo.origen" => ["nullable", "max:30"],
            "tramo.destino" => ["nullable", "max:30"],
            "cadenamiento.kilometro" => ["digits_between:1,4"],
            "cadenamiento.metro" => ["digits_between:1,3"]
        ];

        $catalogo_simple_rules = (new CatalogoSimpleRequest())->rules($tipo_domicilio_rule);
        $catalogo_simple = \Gealtec::iter_rules($catalogo_simple_rules, ["termino_generico", "margen"]);

        return array_merge(
            $rules,
            $catalogo_simple
        );
    }


}
