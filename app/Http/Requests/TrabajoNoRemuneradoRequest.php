<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TrabajoNoRemuneradoRequest extends FormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "integrantes.*" => "numeric"
        ];

        $clave_nombre_rules = (new CatalogoSimpleClaveRequest())->rules();
        $catalogo_simple_rules = (new CatalogoSimpleRequest())->rules();

        $campos_clave_nombre = \Gealtec::iter_rules($clave_nombre_rules, ["tipo_trabajo"]);
        $campos_catalogo_simple = \Gealtec::iter_rules($catalogo_simple_rules, ["excepcion"]);

        return array_merge(
            $rules,
            $campos_clave_nombre,
            $campos_catalogo_simple
        );
    }
}
