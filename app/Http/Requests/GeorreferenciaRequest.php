<?php

namespace App\Http\Requests;

use App\Rules\Point;
use Illuminate\Foundation\Http\FormRequest;

class GeorreferenciaRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "fecha" => ["required", "date_format:Y-m-d\TH:i:s.000\Z"],
            "ubicacion" => ["required", new Point()]
        ];
    }
}
