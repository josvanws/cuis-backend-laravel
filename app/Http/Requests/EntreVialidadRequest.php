<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EntreVialidadRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "nombre_vialidad" => ["nullable","max:60"],
        ];

        $catalogo_simple_rules = (new CatalogoSimpleRequest())->rules("nullable");
        $catalogo_simple = \Gealtec::iter_rules($catalogo_simple_rules, ["tipo_vialidad"]);

        return array_merge(
            $rules,
            $catalogo_simple
        );
    }
}
