<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CatalogoSimpleRequest extends FormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @param string|callable $required_if
     * @return array
     */
    public function rules($required_if = null)
    {
        return [
            "id" => [$required_if ?: "required", "numeric"],
            "nombre" => [$required_if ?: "required"]
        ];
    }
}
