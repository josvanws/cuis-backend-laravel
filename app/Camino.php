<?php

namespace App;

use App\Transformers\CaminoTransformer;
use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;

class Camino extends Model implements Transformable
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function direccion()
    {
        return $this->belongsTo('App\Direccion');
    }

    public function margen()
    {
        return $this->belongsTo('App\TipoMargen');
    }

    public function tipoCamino()
    {
        return $this->belongsTo('App\TipoCamino');
    }
    public function transformer()
    {
        return CaminoTransformer::class;
    }
}
