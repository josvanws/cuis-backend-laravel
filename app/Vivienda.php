<?php

namespace App;

use App\Transformers\ViviendaTransformer;
use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;

class Vivienda extends Model implements Transformable
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function tipoVivienda(){
        return $this->belongsto('App\TipoVivienda');
    }

    public function hogares()
    {
        return $this->hasMany('App\Hogar');
    }

    public function direccion()
    {
        return $this->belongsTo('App\Direccion');
    }

    public function transformer()
    {
        return ViviendaTransformer::class;
    }
}
