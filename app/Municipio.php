<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function estado()
    {
        return $this->belongsTo('App\Estado');
    }

    public function localidades()
    {
        return $this->hasMany('App\Localidad', 'municipio_id');
    }
}
