<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoCamino extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;


    public function caminos() {
        return $this->hasMany('App\Camino');
    }

}
