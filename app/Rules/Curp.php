<?php

namespace App\Rules;

use App\Helpers\ValidaCurp;
use Illuminate\Contracts\Validation\Rule;

class Curp implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $resp = ValidaCurp::validar($value);
        return $resp['Estatus'];
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'No se encuentra la CURP';
    }
}
