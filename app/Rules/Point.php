<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Point implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $is_type_point = @$value["type"] === "Point";
        $coordinates = @$value["coordinates"] ?: [];
        $longitude = @$coordinates[0];
        $latitude = @$coordinates[1];
        $is_longitude_valid = $longitude >= -180 && $longitude <= 180;
        $is_latitude_valid = $latitude >= -90 && $latitude <= 90;
        $are_coordinates_valid = $is_latitude_valid && $is_longitude_valid;
        return $is_type_point && $are_coordinates_valid;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'No es un punto válido.';
    }
}
