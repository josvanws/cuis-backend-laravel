<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class Levantamiento extends Model
{
    use DatabaseMigrations; // To reset the database after each test, add:
    protected $connection = 'mongodb';
    protected $collection = 'levantamiento';
    protected $guarded = [];
}
