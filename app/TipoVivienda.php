<?php

namespace App;

use App\Transformers\TipoViviendaTransformer;
use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;

class TipoVivienda extends Model implements Transformable
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function viviendas()
    {
        return $this->hasMany('App\Vivienda');
    }

    /**
     * @inheritDoc
     */
    public function transformer()
    {
        return TipoViviendaTransformer::class;
    }
}
