<?php

namespace App;

use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;

class TipoVialidad extends Model implements Transformable
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tipo_vialidades';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function vialidades()
    {
        return $this->hasMany('App\Vialidad');
    }

    public function transformer()
    {
        return TipoVialidadTransformer::class;
    }
}
