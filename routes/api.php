<?php

use Illuminate\Http\Request;
use App\Estado;
use App\Vialidad;
use App\Asentamiento;
use App\Carretera;
use App\Camino;
use App\Direccion;
use App\Hogar;
use App\Vivienda;
use App\Integrante;
use Phaza\LaravelPostgis\Geometries\Point;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/** Asentamientos*/
//Route::get('/asentamientos', 'AsentamientoController@index');
//Route::get('/asentamientos/{asentamiento}', 'AsentamientoController@show');
//Route::post('/asentamientos', 'AsentamientoController@store');
//Route::put('/asentamientos/{asentamiento}', 'AsentamientoController@update');
//Route::delete('/asentamientos/{asentamiento}', 'AsentamientoController@destroy');

Route::apiResource('/asentamientos', 'AsentamientoController');

/** Caminos */
//Route::get('/caminos', 'CaminoController@index');
//Route::get('/caminos/{camino}', 'CaminoController@show');
//Route::post('/caminos', 'CaminoController@store');
//Route::put('/caminos/{camino}', 'CaminoController@update');
//Route::delete('/caminos/{camino}', 'CaminoController@destroy');

Route::apiResource('/caminos', 'CaminoController');
/** Carreteras */
//Route::get('/carreteras', 'CarreteraController@index');
//Route::get('/carreteras/{carretera}', 'CarreteraController@show');
//Route::post('/carreteras', 'CarreteraController@store');
//Route::put('/carreteras/{carretera}', 'CarreteraController@update');
//Route::delete('/carreteras/{carretera}', 'CarreteraController@destroy');

Route::apiResource('/carreteras', 'CarreteraController');

/** Direcciónes */
//Route::get('/direcciones', 'DireccionController@index');
//Route::get('/direcciones/{direccion}', 'DireccionController@show');
//Route::post('/direcciones', 'DireccionController@store');
//Route::put('/direcciones/{direccion}', 'DireccionController@update');
//Route::delete('/direcciones/{direccion}', 'DireccionController@destroy');

Route::apiResource('direcciones', 'DireccionController')->parameters(['direcciones' => 'direccion']);

/** Hogares */
//Route::get('/hogares', 'HogarController@index');
//Route::get('/hogares/{hogar}', 'HogarController@show');
//Route::post('/hogares', 'HogarController@store');
//Route::put('/hogares/{hogar}', 'HogarController@update');
//Route::delete('/hogares/{hogar}', 'HogarController@destroy');

Route::apiResource('/hogares', 'HogarController')->parameters(['hogares' =>'hogar']);

/** Integrantes */
//Route::get('/integrantes', 'IntegranteController@index');
//Route::get('/integrantes/{integrante}', 'IntegranteController@show');
//Route::post('/integrantes', 'IntegranteController@store');
//Route::put('/integrantes/{integrante}', 'IntegranteController@update');
//Route::delete('/integrantes/{integrante}', 'IntegranteController@destroy');

Route::apiResource('/integrantes', 'IntegranteController')->parameters(['integrantes' => 'integrante']);

/** Vialidades */
//Route::get('/vialidades', 'VialidadController@index');
//Route::get('/vialidades/{vialidad}', 'VialidadController@show');
//Route::post('/vialidades', 'VialidadController@store');
//Route::put('/vialidades/{vialidad}', 'VialidadController@update');
//Route::delete('/vialidades/{vialidad}', 'vialidadController@destroy');

Route::apiResource('/vialidades', 'VialidadController')->parameters(['vialidades' => 'vialidad']);

/** Viviendas */
//Route::get('/viviendas', 'ViviendaController@index');
//Route::get('/viviendas/{vivienda}', 'ViviendaController@show');
//Route::post('/viviendas', 'ViviendaController@store');
//Route::put('/viviendas/{vivienda}', 'ViviendaController@update');
//Route::delete('/viviendas/{vivienda}', 'ViviendaController@destroy');

Route::apiResource('/viviendas', 'ViviendaController')->parameters(['viviendas' => 'vivienda']);

Route::post('levantamiento', 'LevantamientoController@index');
