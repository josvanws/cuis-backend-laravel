<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/friends/{quien}', function ($quien) {
    return Route::currentRouteName();
})->name('inicio')->middleware('check.age');

Route::get('/namedroute', function(){
    return route('inicio',['quien'=>'carlios']);
});

Route::get('user/{name}', function($name){
    return $name;
})->where('name','[A-Za-z]+');

Route::get('home', function(){
    return 'home';
});

Route::get('/test/requestClosure/{idx}', function(Illuminate\Http\Request $request){
    // Request methods
    $rq = [
        'path' => $request->path(),
        'is' => $request->is('test/*'),
        'url' => $request->url(),
        'fullUrl' => $request->fullUrl(),
        'method' => $request->method(),
        'inputData' => $request->all(),
        'param' => $request->idx,
        'bodyparam' => $request->nombre,
        'name' => $request->input('nombre'),
        'id' => $request->input('id'),
        'allInputData' => $request->input(),
        'queryStringParam' => $request->query('modelo'),
        'JSONInputValues' => $request->input('coords.lat')
    ];
   return $rq;
});

Route::get('/', function(){
    return response([1,2,3])
        ->header('X-Header-One', 'Mi header x1')
        ->header('X-Header-Two', 'Mi header x2');
});





Route::get('/asentamiento/{asentamiento}', function (App\Asentamiento $asentamiento) {
    return $asentamiento->tipoAsentamiento;
});

Route::get('/tipoAsentamiento/{tipoAsentamiento}', function (App\TipoAsentamiento $tipoAsentamiento) {
    return $tipoAsentamiento->asentamientos;
});

Route::get('/municiposDeEstado/{estado}', function (App\Estado $estado) {
    return $estado->municipios;
});

Route::get('/estadoDeMunicipio/{municipio}', function (App\Municipio $municipio) {
    return $municipio->estado;
});

Route::get('/localidadesDeMunicipio/{municipio}', function (App\Municipio $municipio) {
    return $municipio->localidades;
});

Route::get('/tipoVialidad/{vialidad}', function (App\Vialidad $vialidad) {
    return $vialidad->tipoVialidades;
});

Route::get('/vialidadesDelTipo/{tipoVialidad}', function (App\TipoVialidad $tipoVialidad) {
    return $tipoVialidad->vialidades;
});
/* carreteras */
Route::get('/carreteraDerechot/{carretera}', function (App\Carretera $carretera) {
    return $carretera->derechoTransito;
});

Route::get('/carreteraTipoAdmon/{carretera}', function (App\Carretera $carretera) {
    return $carretera->tipoAdministraciones;
});

Route::get('/dtCarretera/{dt}', function (App\DerechoTransito $dt) {
    return $dt->carreteras;
});

Route::get('/taCarretera/{ta}', function (App\TipoAdministracion $ta) {
    return $ta->carreteras;
});

Route::get('/carreteraDireccion/{carretera}', function (App\Carretera $carretera) {
    return $carretera->direccion;
});
/* caminos */
Route::get('/caminosTipoCamino/{camino}', function (App\Camino $camino) {
    return $camino->tipoCamino;
});

Route::get('/caminosTipoMrgen/{camino}', function (App\Camino $camino) {
    return $camino->margen;
});

Route::get('/tpCaminos/{dt}', function (App\TipoCamino $dt) {
    return $dt->caminos;
});

Route::get('/tmCaminos/{ta}', function (App\TipoMargen $ta) {
    return $ta->caminos;
});

Route::get('/caminosDireccion/{camino}', function (App\Camino $camino) {
    return $camino->direccion;
});

/** Dirección */
Route::get('direccionLocalidad/{direccion}', function (App\Direccion $direccion) {
    return $direccion->localidad;
});

Route::get('direccionVialidad/{direccion}', function (App\Direccion $direccion) {
    return $direccion->vialidad;
});

Route::get('direccionEntreVialidad1/{direccion}', function (App\Direccion $direccion) {
    return $direccion->entre_vialidad1;
});
Route::get('direccionEntreVialidad2/{direccion}', function (App\Direccion $direccion) {
    return $direccion->entre_vialidad2;
});
Route::get('direccionVialidadPosterior/{direccion}', function (App\Direccion $direccion) {
    return $direccion->vialidad_posterior;
});

Route::get('direccionAsentamiento/{direccion}', function (App\Direccion $direccion) {
    return $direccion->asentamiento;
});

Route::get('direccionVivienda/{direccion}', function (App\Direccion $direccion) {
    return $direccion->vivienda;
});

Route::get('parentescoIntegrantes/{parentesco}', function (App\Parentesco $parentesco) {
    return $parentesco->integrantes;
});

Route::get('hogarIntegrante/{hogar}', function (App\Hogar $hogar){
    return $hogar->integrantes;
});

Route::get('hogarVivienda/{hogar}', function (App\Hogar $hogar) {
    return $hogar->vivienda;
});

Route::get('integranteParentesco/{integrante}', function (App\Integrante $integrante) {
    return $integrante->parentesco;
});

Route::get('integranteHogar/{integrante}', function (App\Integrante $integrante) {
    return $integrante->hogar;
});

Route::get('integranteEstado/{integrante}', function (App\Integrante $integrante) {
    return $integrante->estadoNacimiento;
});


Route::get('viviendaDireccion/{vivienda}', function (App\Vivienda $vivienda) {
    return $vivienda->direccion;
});

Route::get('viviendaTipoVivienda/{vivienda}', function (App\Vivienda $vivienda) {
    return $vivienda->tipoVivienda;
});

Route::get('tipoViviendaVivienda/{tipovivienda}', function (App\tipoVivienda $tipovivienda) {
    return $tipovivienda->viviendas;
});

Route::fallback(function(){
    return " ¡Chales! No se encontró nada.";
});
