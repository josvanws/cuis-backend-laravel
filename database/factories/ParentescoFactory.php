<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Parentesco;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Parentesco::class, function (Faker $faker) {
    return [
    'nombre' => Str::random(10)
    ];
});
