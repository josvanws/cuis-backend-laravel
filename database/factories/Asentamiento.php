<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Asentamiento;
use App\TipoAsentamiento;
use Faker\Generator as Faker;

$factory->define(Asentamiento::class, function (Faker $faker) {
    return [
        'nombre' => $faker->name,
        'tipo_asentamiento_id' => function() {
            return factory(TipoAsentamiento::class)->create()->id;
        }
    ];
});
