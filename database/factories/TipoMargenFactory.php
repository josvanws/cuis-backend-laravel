<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\TipoMargen;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(TipoMargen::class, function (Faker $faker) {
    return [
        'nombre' => Str::random(10),
    ];
});
