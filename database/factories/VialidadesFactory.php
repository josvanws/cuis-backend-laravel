<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Vialidad;
use App\TipoVialidad;
use Faker\Generator as Faker;

$factory->define(Vialidad::class, function (Faker $faker) {
    return [
        'nombre' => $faker->name,
        'tipo_vialidad_id' => function() {
            return factory(TipoVialidad::class)->create()->id;
        }
    ];
});
