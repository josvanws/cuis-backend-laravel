<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Municipio;
use Faker\Generator as Faker;

$factory->define(Municipio::class, function (Faker $faker) {
    return [
        'nombre' => $faker->name,
        'clave' => Str::random(10),
    ];
});
