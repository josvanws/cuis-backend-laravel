<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DerechoTransito;
use Faker\Generator as Faker;

$factory->define(DerechoTransito::class, function (Faker $faker) {
    return [
        'nombre' => $faker->name,
    ];
});
