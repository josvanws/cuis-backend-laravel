<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Estado;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Estado::class, function (Faker $faker) {
    return [
        'nombre' => $faker->name,
        'clave' => Str::random(10),
    ];
});
