<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Integrante;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Integrante::class, function (Faker $faker) {
    return [
        'apellido_paterno' => $faker->lastName,
        'apellido_materno' =>  $faker->lastName,
        'nombre' =>  $faker->firstName,
        'curp' => Str::random(18),
        'fecha_nacimiento' => now(),
        'sexo' => rand(0,1) ? '1' : '2',
        'hogar_id' => $faker->randomDigitNotNull,
        'estado_nacimiento_id' => $faker->randomDigitNotNull
    ];
});
