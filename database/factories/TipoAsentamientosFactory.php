<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\TipoAsentamiento;
use Faker\Generator as Faker;

$factory->define(TipoAsentamiento::class, function (Faker $faker) {
    return [
        'nombre' => $faker->name,
    ];
});
