<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Carretera;
use App\Direccion;
use App\DerechoTransito;
use App\TipoAdministracion;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Carretera::class, function (Faker $faker) {
    return [
        'direccion_id' => $faker->randomDigitNotNull,
        'tipo_administracion_id' => $faker->randomDigitNotNull,
        'derecho_transito_id' => $faker->randomDigitNotNull,
        'codigo' => Str::random(5),
        'tramo_origen' => Str::random(10),
        'tramo_destino' => Str::random(10),
        'km' => $faker->randomNumber(3),
        'metro' => $faker->randomNumber(3)
    ];
});
