<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Vivienda;
use Faker\Generator as Faker;

$factory->define(Vivienda::class, function (Faker $faker) {
    return [
        'direccion_id' => $faker->randomDigitNotNull,
        'tipo_vivienda_id' => $faker->randomDigitNotNull,
        'numero_personas' => $faker->randomDigitNotNull,
        'numero_hogares' => $faker->numberBetween(1,2)
    ];
});
