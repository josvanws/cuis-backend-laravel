<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Hogar;
use Faker\Generator as Faker;

$factory->define(Hogar::class, function (Faker $faker) {
    return [
        'vivienda_id' => $faker->randomDigitNotNull,
        'numero_integrantes' => $faker->randomDigitNotNull
    ];
});
