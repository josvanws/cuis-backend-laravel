<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\TipoAdministracion;
use Faker\Generator as Faker;

$factory->define(TipoAdministracion::class, function (Faker $faker) {
    return [
        'nombre' => $faker->name,
    ];
});
