<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\TipoCamino;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(TipoCamino::class, function (Faker $faker) {
    return [
        'nombre' => Str::random(10),
    ];
});
