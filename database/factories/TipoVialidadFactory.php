<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\TipoVialidad;
use Faker\Generator as Faker;

$factory->define(TipoVialidad::class, function (Faker $faker) {
    return [
        'nombre' => $faker->name,
    ];
});
