<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\TipoVivienda;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(TipoVivienda::class, function (Faker $faker) {
    return [
        'nombre' => Str::random(10),
    ];
});
