<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Camino;
use App\Direccion;
use App\TipoCamino;
use App\TipoMargen;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Camino::class, function (Faker $faker) {
    return [
        'direccion_id' => $faker->randomDigitNotNull,
        'tipo_margen_id' =>$faker->randomDigitNotNull,
        'tipo_camino_id' => $faker->randomDigitNotNull,

        'tramo_origen' => Str::random(10),
        'tramo_destino' => Str::random(10),
        'km' => $faker->randomNumber(3),
        'metro' => $faker->randomNumber(3)
    ];
});
