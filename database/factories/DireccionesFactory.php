<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Direccion;
use App\Localidad;
use App\Vialidad;
use App\Asentamiento;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Direccion::class, function (Faker $faker) {
    return [
        'localidad_id' => $faker->randomDigitNotNull,
        'clave_ageb' => Str::random(3),
        'clave_manzana' =>  Str::random(3),
        'vialidad_id' => $faker->randomDigitNotNull,
        'num_exterior' => $faker->randomNumber(3),
        'letra' => $faker->randomLetter,
        'num_exterior_anterior' => $faker->randomNumber(3),
        'num_interior' => $faker->randomNumber(2),
        'letra_interior' => $faker->randomLetter,
        'cp' => $faker->postcode,
        'asentamiento_id' => $faker->randomDigitNotNull,
        'entre_vialidad1_id' => $faker->randomDigitNotNull,
        'entre_vialidad2_id' => $faker->randomDigitNotNull,
        'vialidad_posterior_id' => $faker->randomDigitNotNull,
        'referencias' => $faker->text(100),

    ];
});
