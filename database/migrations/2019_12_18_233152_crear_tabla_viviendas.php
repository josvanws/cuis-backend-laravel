<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaViviendas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('viviendas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('direccion_id');
            $table->smallInteger('tipo_vivienda_id');
            $table->smallInteger('numero_personas');
            $table->smallInteger('numero_hogares');

       
            $table->foreign('direccion_id')->references('id')->on('direcciones');
            $table->foreign('tipo_vivienda_id')->references('id')->on('tipo_viviendas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('viviendas');
    }
}
