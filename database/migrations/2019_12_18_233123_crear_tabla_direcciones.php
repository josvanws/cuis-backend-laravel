<?php

use Illuminate\Database\Migrations\Migration;
use Phaza\LaravelPostgis\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaDirecciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direcciones', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('localidad_id');
            $table->string('clave_ageb');
            $table->string('clave_manzana');
            $table->integer('vialidad_id');
            $table->smallInteger('num_exterior');
            $table->string('letra');
            $table->smallInteger('num_exterior_anterior');
            $table->smallInteger('num_interior');
            $table->string('letra_interior'); 
            $table->string('cp'); 
            $table->integer('asentamiento_id');
            $table->integer('entre_vialidad1_id');
            $table->integer('entre_vialidad2_id');
            $table->integer('vialidad_posterior_id');
            $table->string('referencias');
            $table->point('ubicacion')->nullable();

            $table->foreign('localidad_id')->references('id')->on('localidades');
            $table->foreign('vialidad_id')->references('id')->on('vialidades');
            $table->foreign('asentamiento_id')->references('id')->on('asentamientos');
            $table->foreign('entre_vialidad1_id')->references('id')->on('vialidades');
            $table->foreign('entre_vialidad2_id')->references('id')->on('vialidades');
            $table->foreign('vialidad_posterior_id')->references('id')->on('vialidades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direcciones');
    }
}
