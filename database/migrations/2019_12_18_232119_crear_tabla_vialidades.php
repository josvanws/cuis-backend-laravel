<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaVialidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vialidades', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('tipo_vialidad_id');
            $table->string('nombre');

            $table->foreign('tipo_vialidad_id')->references('id')->on('tipo_vialidades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vialidades');
    }
}
