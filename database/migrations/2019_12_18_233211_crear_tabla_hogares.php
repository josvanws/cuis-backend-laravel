<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaHogares extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hogares', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vivienda_id');
            $table->smallInteger('numero_integrantes');

            
            $table->foreign('vivienda_id')->references('id')->on('viviendas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hogares');
    }
}
