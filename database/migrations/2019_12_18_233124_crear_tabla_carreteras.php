<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaCarreteras extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carreteras', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('direccion_id');
            $table->smallInteger('tipo_administracion_id');
            $table->smallInteger('derecho_transito_id');
            $table->string('codigo');
            $table->string('tramo_origen');
            $table->string('tramo_destino');
            $table->string('km');
            $table->string('metro');

            $table->foreign('direccion_id')->references('id')->on('direcciones');
            $table->foreign('tipo_administracion_id')->references('id')->on('tipo_administraciones');
            $table->foreign('derecho_transito_id')->references('id')->on('derechos_transito');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carreteras');
    }
}
