<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaCaminos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('caminos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('direccion_id');
            $table->string('tramo_origen');
            $table->string('tramo_destino');
            $table->smallInteger('tipo_margen_id');
            $table->string('km');
            $table->string('metro');
            $table->smallInteger('tipo_camino_id');

            $table->foreign('direccion_id')->references('id')->on('direcciones');
            $table->foreign('tipo_margen_id')->references('id')->on('tipo_margenes');
            $table->foreign('tipo_camino_id')->references('id')->on('tipo_caminos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('caminos');
    }
}
