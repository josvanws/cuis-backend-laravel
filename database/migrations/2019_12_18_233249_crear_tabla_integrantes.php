<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaIntegrantes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('integrantes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('apellido_paterno');
            $table->string('apellido_materno');
            $table->string('nombre');

            $table->string('curp');
            $table->date('fecha_nacimiento');
            $table->string('sexo');
            $table->integer('hogar_id');
            $table->smallInteger('estado_nacimiento_id');


            $table->unique('curp');
            $table->foreign('hogar_id')->references('id')->on('hogares');
            $table->foreign('estado_nacimiento_id')->references('id')->on('estados');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('integrantes');
    }
}
