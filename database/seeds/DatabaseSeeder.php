<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Estado::class, 30)->create()->each(function ($estado) {
            $municipio = factory(App\Municipio::class)->make();
            $estado->municipios()->save($municipio);
            // intentar un savemany
            $municipio->localidades()->save(factory(App\Localidad::class)->make());
        });

        factory(App\Vialidad::class, 10)->create();
        factory(App\Asentamiento::class, 10)->create();
        factory(App\DerechoTransito::class, 10)->create();
        factory(App\TipoAdministracion::class, 10)->create();
        factory(App\TipoCamino::class, 10)->create();
        factory(App\TipoMargen::class, 10)->create();
        factory(App\Direccion::class, 10)->create();
        factory(App\Carretera::class, 10)->create();
        factory(App\Camino::class, 10)->create();
        factory(App\TipoVivienda::class, 10)->create();
        factory(App\Vivienda::class, 10)->create();
        factory(App\Hogar::class, 10)->create();

        factory(App\Integrante::class, 10)->create();



    }
}
