<?php

return [
    'ws_validacurp_url' => env('WS_VALIDARCURP_URL'),
    'ws_validacurp_usuario' => env('WS_VALIDARCURP_USUARIO'),
    'ws_validacurp_clave' => env('WS_VALIDARCURP_CLAVE')
];
